package com.example.challenge3.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.example.challenge3.data.Detail
import com.example.challenge3.databinding.FragmentThirdBinding

class ThirdFragment : Fragment() {
    private var  _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<Detail>(FourthFragment.EXTRA_DETAIL)
            ?.observe(viewLifecycleOwner) { detail ->
                binding.tvAge.visibility = View.VISIBLE
                val setAge = "Usia Anda: ${detail.age} (${checkAge(detail.age!!)})"
                binding.tvAge.text = setAge

                binding.tvAddress.visibility = View.VISIBLE
                val setAddress = "Alamat Anda : ${detail.address}"
                binding.tvAddress.text = setAddress

                binding.tvProfesi.visibility = View.VISIBLE
                val setProfesi = "Profesi Anda : ${detail.prof}"
                binding.tvProfesi.text = setProfesi
            }

        val getName = ThirdFragmentArgs.fromBundle(arguments as Bundle).name
        binding.tvName.text = "Nama anda : $getName"

        binding.btnGoToFrag4.setOnClickListener {
            val action = ThirdFragmentDirections.actionThirdFragmentToFourthFragment()
            view.findNavController().navigate(action)
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    fun checkAge(age: Int) : String{
        var hasil = ""

        if (age % 2 == 0 ) {
            hasil = "genap"
        } else{
            hasil = "ganjil"
        }
        return hasil
    }
}