package com.example.challenge3.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import com.example.challenge3.R
import com.example.challenge3.databinding.FragmentFourthBinding
import com.example.challenge3.databinding.FragmentSecondBinding

class SecondFragment : Fragment() {
    private var  _binding: FragmentSecondBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentSecondBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnGoToFrag3.setOnClickListener {
            val inputName = binding.edtName.text.toString()
            if (inputName.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "nama masih kosong", Toast.LENGTH_SHORT).show()
            } else {
                val action = SecondFragmentDirections.actionSecondFragmentToThirdFragment(inputName)
                view.findNavController().navigate(action)
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}