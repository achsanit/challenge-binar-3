package com.example.challenge3.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.example.challenge3.R
import com.example.challenge3.data.Detail
import com.example.challenge3.databinding.FragmentFirstBinding
import com.example.challenge3.databinding.FragmentFourthBinding

class FourthFragment : Fragment() {
    private var  _binding: FragmentFourthBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentFourthBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnBackToFrag3.setOnClickListener {
            val getAge = binding.edtUsia.text.toString()
            val getAddress = binding.edAlamat.text.toString()
            val getProfesi = binding.edtPekerjaan.text.toString()

            if (getAddress.isNullOrEmpty()||getAge.isNullOrEmpty()||getProfesi.isNullOrEmpty()) {
                Toast.makeText(requireContext(), "masih ada field yang kosong", Toast.LENGTH_SHORT).show()
            } else {
                val detail = Detail(getAge.toInt(), getAddress, getProfesi)
                findNavController().previousBackStackEntry?.savedStateHandle?.set(EXTRA_DETAIL, detail)
                findNavController().navigateUp()
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    companion object {
        val EXTRA_DETAIL = "EXTRA_DETAIL"
    }
}